﻿namespace Olekstra.Util.AwsDynamoDbUploader
{
    using System;

    public class Options
    {
        public string AwsAccessKey { get; set; }

        public string AwsSecretKey { get; set; }

        public string AwsRegion { get; set; }

        public string Table { get; set; }

        public string File { get; set; }

        public int Speed { get; set; }
    }
}
