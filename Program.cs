﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Microsoft.Extensions.Configuration;

namespace Olekstra.Util.AwsDynamoDbUploader
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddUserSecrets<Program>()
                .AddCommandLine(args)
                .Build();

            var options = new Options
            {
                AwsAccessKey = configuration["AwsAccessKey"],
                AwsSecretKey = configuration["AwsSecretKey"],
                AwsRegion = configuration["AwsRegion"],
                Speed = int.Parse(configuration["Speed"]),
                File = configuration["File"],
                Table = configuration["Table"]
            };

            new Program().Run(options).Wait();

            Console.WriteLine();
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        public async Task Run(Options options)
        {
            var client = new AmazonDynamoDBClient(
                options.AwsAccessKey,
                options.AwsSecretKey,
                Amazon.RegionEndpoint.GetBySystemName(options.AwsRegion));

            var delay = 1000 / options.Speed;
            var stopwatch = new Stopwatch();

            var counter = 0;

            using (var stream = new StreamReader(options.File))
            {
                var csvConfig = new CsvHelper.Configuration.CsvConfiguration
                {
                    HasHeaderRecord = true
                };
                using (var file = new CsvHelper.CsvReader(stream, csvConfig))
                {
                    var key = new Dictionary<string, AttributeValue>();
                    var values = new Dictionary<string, AttributeValueUpdate>();
                    while (file.Read())
                    {
                        stopwatch.Restart();

                        key.Clear();
                        values.Clear();

                        key[file.FieldHeaders[0]] = new AttributeValue(file.CurrentRecord[0]);
                        ////key[file.FieldHeaders[1]] = new AttributeValue(file.CurrentRecord[1]);
                        for (var i = 1; i < file.FieldHeaders.Length; i++)
                        {
                            values[file.FieldHeaders[i]] = new AttributeValueUpdate(
                                new AttributeValue(file.CurrentRecord[i]),
                                AttributeAction.PUT);
                        }

                        await client.UpdateItemAsync(options.Table, key, values);

                        counter++;

                        Console.CursorLeft = 0;
                        Console.Write(counter);

                        var elapsed = stopwatch.ElapsedMilliseconds;
                        if (elapsed < delay)
                        {
                            await Task.Delay(delay - (int)stopwatch.ElapsedMilliseconds);
                        }
                    }
                }

                Console.WriteLine();
                Console.WriteLine("Done");
            }
        }
    }
}
