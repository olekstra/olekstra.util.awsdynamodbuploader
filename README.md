﻿# AWS DynamoDb Uploader

Умеет загружать данные в таблицу AWS DynamoDb из csv-файла.

Загрузка делается методом "update" (обновляются указанные атрибуты, остальные остаются без изменений).

## Требования к csv-файлу

* Первая строка - имена полей;
* Первое поле - ключ в таблице (PartitionKey), второй ключ (RangeKey) пока не поддерживается;
* Разделитель полей - запятая (КБМП!), ну и дальше всё согласно классике csv ([Wiki](https://ru.wikipedia.org/wiki/CSV)).

## Запуск

1. Задайте параметры подключения к AWS в `appsettings.json` или через [app-secrets](https://docs.asp.net/en/latest/security/app-secrets.html)
2. Не забудьте указать [правильный регион](http://docs.aws.amazon.com/general/latest/gr/rande.html#ddb_region) (`AwsRegion`)!
3. Имя таблицы и имя файла удобно указывать уже при запуске:
    > `dotnet run --table MyTable --file MyFile.csv`

